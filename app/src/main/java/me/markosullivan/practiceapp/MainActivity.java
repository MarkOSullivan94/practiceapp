package me.markosullivan.practiceapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

/**
 * Plan
 * Expandable items for Rounds and Performance.
 *
 * Rounds
 * -> Overview (Tabs: Map vs Scorecard)
 * -> Stats (Tabs: Round vs Season)
 *
 * Performance
 * -> Clubs (Tabs: Distance vs Usage)
 * -> Tee Shots
 * -> Approaches
 * -> Short Game
 * -> Putting
 */

public class MainActivity extends AppCompatActivity {

	Drawer result;
	AccountHeader headerResult;
	public Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
		buildAccountHeader();
        openFragment(new RetrofitFragment());

        new DrawerBuilder().withActivity(this).build();

		SecondaryDrawerItem expandItem1 = new SecondaryDrawerItem().withIdentifier(2).withName("Retrofit 2.x");
		SecondaryDrawerItem expandItem2 = new SecondaryDrawerItem().withIdentifier(3).withName("Glide");
		SecondaryDrawerItem expandItem3 = new SecondaryDrawerItem().withIdentifier(4).withName("Picasso");
		ExpandableDrawerItem expandableDrawer1 = new ExpandableDrawerItem().withIdentifier(1).withName("Libraries").withSubItems(expandItem1, expandItem2, expandItem3);
		PrimaryDrawerItem primaryDrawer1 = new PrimaryDrawerItem().withIdentifier(5).withName("About");
		PrimaryDrawerItem primaryDrawer2 = new PrimaryDrawerItem().withIdentifier(6).withName("Help");
        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
				.withAccountHeader(headerResult)
                .addDrawerItems(
                        expandableDrawer1,
						new DividerDrawerItem(),
						primaryDrawer1,
						primaryDrawer2
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        // do something with the clicked item :D
                        switch ((int)drawerItem.getIdentifier()){

                            case 1:
								System.out.println("Libraries clicked");
								break;

                            case 2:
								openFragment(new RetrofitFragment());
								result.closeDrawer();
                                break;

							case 3:
								Toast.makeText(MainActivity.this, "Glide example coming soon", Toast.LENGTH_LONG).show();
								break;

							case 4:
								Toast.makeText(MainActivity.this, "Picasso example coming soon", Toast.LENGTH_LONG).show();

                            case 5:
								openFragment(new FragmentTwo());
								result.closeDrawer();
                                break;

							case 6:
								Toast.makeText(MainActivity.this, "Help? Lol JK", Toast.LENGTH_SHORT).show();
						}
                        return true;
                    }
                })
                .build();
    }

	private void openFragment(final Fragment mFragment){
		FragmentTransaction mFragmentTransaction = getSupportFragmentManager().beginTransaction();
		mFragmentTransaction.replace(R.id.fl_fragment_container, mFragment);
		mFragmentTransaction.addToBackStack(null);
		mFragmentTransaction.commit();
	}

	private void buildAccountHeader(){
		 headerResult = new AccountHeaderBuilder()
				.withActivity(this)
				.withHeaderBackground(R.drawable.header)
				.addProfiles(
						new ProfileDrawerItem().withName("Mark O'Sullivan").withEmail("markosullivan@email.com")
				)
				.withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
					@Override
					public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
						return false;
					}
				})
				.build();
	}
}
