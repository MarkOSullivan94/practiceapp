package me.markosullivan.practiceapp.api;

import java.util.List;

import me.markosullivan.practiceapp.pojo.Contributors;
import me.markosullivan.practiceapp.pojo.User;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Mark O'Sullivan on 22/08/16.
 * Shot Scope
 * markosullivan@shotscope.com
 */
public interface GitHubApi {

    @GET("repos/{owner}/{repo}/contributors")
    Call<List<Contributors>> repoContributors(
            @Path("owner") String owner,
            @Path("repo") String repo);

    @GET("/users/{user}")
    Call<User> getUser(@Path("user") String user);


    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://api.github.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
