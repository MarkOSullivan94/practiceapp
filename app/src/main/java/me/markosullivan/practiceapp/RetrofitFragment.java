package me.markosullivan.practiceapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.gigamole.navigationtabstrip.NavigationTabStrip;

import java.util.Arrays;
import java.util.List;

import me.markosullivan.practiceapp.api.GitHubApi;
import me.markosullivan.practiceapp.pojo.Contributors;
import me.markosullivan.practiceapp.pojo.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mark O'Sullivan on 30/08/16.
 * Shot Scope
 * markosullivan@shotscope.com
 */
public class RetrofitFragment extends Fragment {

    private ViewPager mViewPager;
    private NavigationTabStrip mNavigationTabStrip;
	private ActionBar actionBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_retrofit, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewPager = (ViewPager) view.findViewById(R.id.vp);
        mViewPager.setAdapter(new RetrofitPagerAdapter(getFragmentManager()));
        mNavigationTabStrip = (NavigationTabStrip) view.findViewById(R.id.nts);
        mNavigationTabStrip.setViewPager(mViewPager, 1);
        mNavigationTabStrip.setTabIndex(0);
        mNavigationTabStrip.setOnTabStripSelectedIndexListener(new NavigationTabStrip.OnTabStripSelectedIndexListener() {
            @Override
            public void onStartTabSelected(String title, int index) {
                System.out.println("Start Title: " + title);
                System.out.println("Start Index: " + index);
            }

            @Override
            public void onEndTabSelected(String title, int index) {
                System.out.println("End Title: " + title);
                System.out.println("End Index: " + index);
            }
        });
		actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
		//System.out.println("Elevation:" + actionBar.getElevation());
		actionBar.setElevation(0f);
    }

	@Override
	public void onPause() {
		super.onPause();
		float elevation = actionBar.getElevation();
		if(elevation == 0f){
			actionBar.setElevation(14.0f);
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		float elevation = actionBar.getElevation();
		if(elevation == 0f){
			actionBar.setElevation(14.0f);
		}
	}
}
