package me.markosullivan.practiceapp;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Mark O'Sullivan on 02/09/16.
 * Shot Scope
 * markosullivan@shotscope.com
 */
public class RetrofitPagerAdapter extends FragmentStatePagerAdapter{

	public RetrofitPagerAdapter(final FragmentManager fm){
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {
			case 0:
				System.out.println("MainPagerAdapter: Position 0");
				return new RetroGitHubFragment();
			case 1:
				System.out.println("MainPagerAdapter: Position 1");
				return new RetroOpenWeatherFragment();
		}
		return null;
	}

	@Override
	public int getCount() {
		return 2;
	}
}
