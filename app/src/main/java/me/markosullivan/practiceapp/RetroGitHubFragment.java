package me.markosullivan.practiceapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import me.markosullivan.practiceapp.api.GitHubApi;
import me.markosullivan.practiceapp.pojo.Contributors;
import me.markosullivan.practiceapp.pojo.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mark O'Sullivan on 02/09/16.
 * Shot Scope
 * markosullivan@shotscope.com
 */
public class RetroGitHubFragment extends Fragment {

	private Button fetchButton;
	private TextView resultTV;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_retro_github, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		fetchButton = (Button) view.findViewById(R.id.button_fetch);
		resultTV = (TextView) view.findViewById(R.id.result_tv);

		fetchButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				GitHubApi gitHubApi = GitHubApi.retrofit.create(GitHubApi.class);
				Call<List<Contributors>> call = gitHubApi.repoContributors("square", "retrofit");

				call.enqueue(new Callback<List<Contributors>>() {
					@Override
					public void onResponse(Call<List<Contributors>> call, Response<List<Contributors>> response) {
						System.out.println(response.body());
						List<Contributors> gitHubModelList = response.body();
						resultTV.setText(Arrays.toString(gitHubModelList.toArray()));
					}

					@Override
					public void onFailure(Call<List<Contributors>> call, Throwable t) {
						String errorMessage = "ERROR: " + t.getMessage();
						resultTV.setText(errorMessage);
					}
				});

				Call<User> getUserCall = gitHubApi.getUser("vogella");
				getUserCall.enqueue(new Callback<User>() {
					@Override
					public void onResponse(Call<User> call, Response<User> response) {
						int code = response.code();
						if(code == 200) {
							User user = response.body();
							Toast.makeText(getContext(), "Got the user: " + user.name, Toast.LENGTH_LONG).show();
						}
						else {
							Toast.makeText(getContext(), "Did not work: " + String.valueOf(code), Toast.LENGTH_LONG).show();
						}
					}

					@Override
					public void onFailure(Call<User> call, Throwable t) {
						Toast.makeText(getContext(), "Nope", Toast.LENGTH_LONG).show();
					}
				});
			}
		});
	}
}
