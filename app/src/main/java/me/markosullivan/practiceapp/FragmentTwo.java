package me.markosullivan.practiceapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Mark O'Sullivan on 30/08/16.
 * Shot Scope
 * markosullivan@shotscope.com
 */
public class FragmentTwo extends Fragment {

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_two, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
		System.out.println("Elevation: " + actionBar.getElevation());
	}
}
